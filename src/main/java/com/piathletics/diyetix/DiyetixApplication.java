package com.piathletics.diyetix;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class}) // DataBase bağlandı ama neredeki database? "Reason: Failed to determine a suitable driver class" hatası
@ComponentScan(basePackages = {"com.piathletics.diyetix.repository"})
@ComponentScan(basePackages = {"com.piathletics.diyetix.model"})
@ComponentScan(basePackages = {"com.piathletics.diyetix.service"})
@ComponentScan(basePackages = {"com.piathletics.diyetix.dto"})
@ComponentScan(basePackages = {"com.piathletics.diyetix.controller"})

public class DiyetixApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiyetixApplication.class, args);
	}

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.piathletics.diyetix"))
				.build();
	}

}
