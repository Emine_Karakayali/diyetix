package com.piathletics.diyetix.repository;

import com.piathletics.diyetix.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;
@Repository
public interface UserRepository extends MongoRepository<User, UUID> {
    User findByUserNameAndPassword(String email, String password);


}