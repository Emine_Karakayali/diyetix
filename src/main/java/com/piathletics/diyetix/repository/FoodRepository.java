package com.piathletics.diyetix.repository;

import com.piathletics.diyetix.model.Food;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;
@Repository
public interface FoodRepository extends MongoRepository<Food, UUID> {
    Food getById(UUID id);
}
