package com.piathletics.diyetix.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TotalCalorieDTO {

    private double portion;
    private String name;
    private double totalCalorie;

}