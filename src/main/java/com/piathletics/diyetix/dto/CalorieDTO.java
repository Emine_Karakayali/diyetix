package com.piathletics.diyetix.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CalorieDTO {
    @Id
    private UUID id;
    private double portion;



}