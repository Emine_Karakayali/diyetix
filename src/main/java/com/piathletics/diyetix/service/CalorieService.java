package com.piathletics.diyetix.service;

import com.piathletics.diyetix.dto.CalorieDTO;
import com.piathletics.diyetix.dto.TotalCalorieDTO;
import com.piathletics.diyetix.model.Food;
import com.piathletics.diyetix.repository.FoodRepository;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Data
public class CalorieService {
    @Autowired
    FoodRepository foodRepository;

    public List<TotalCalorieDTO> calculateTC(List<CalorieDTO> bmidtos) {
        List<TotalCalorieDTO> totalCaloriesDtos = new ArrayList();

        bmidtos.forEach(bmidto -> {
            double result = 0;
            Food food = foodRepository.getById(bmidto.getId());
            result = (food.getCalorie() * bmidto.getPortion());
            totalCaloriesDtos.add(new TotalCalorieDTO(bmidto.getPortion(), food.getName(), result));
        });
        return totalCaloriesDtos;
    }
}