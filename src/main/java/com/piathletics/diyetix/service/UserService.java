package com.piathletics.diyetix.service;

import com.piathletics.diyetix.dto.LoginDTO;
import com.piathletics.diyetix.dto.UserDTO;
import com.piathletics.diyetix.model.User;
import com.piathletics.diyetix.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;

    public List<User> listAllUser() {

        return userRepository.findAll();
    }

    public String addUser(UserDTO userDTO) {

        User userr = new User();
        userr.setId(UUID.randomUUID());
        userr.setUserName(userDTO.getUserName());
        userr.setPassword(userDTO.getPassword());
        userRepository.save(userr);
        return "success";
    }

    public boolean userByUserName(LoginDTO loginDTO){
        User userr =  userRepository.findByUserNameAndPassword(loginDTO.getUserName(), loginDTO.getPassword());
        if(userr != null){
            return true;
        }
        return false;
    }

    public String updateUser(UserDTO userDTO) {
        User userr = new User();
        userr.setId(userDTO.getId());
        userr.setUserName(userDTO.getUserName());
        userr.setPassword(userDTO.getPassword());
        userRepository.save(userr);
        return "user updated";
    }

    public String deleteUserById(UUID id) {
        userRepository.deleteById(id);
        return "user deleted";
    }
}