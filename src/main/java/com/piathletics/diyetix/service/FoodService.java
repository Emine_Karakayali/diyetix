package com.piathletics.diyetix.service;

import com.piathletics.diyetix.model.Food;
import com.piathletics.diyetix.repository.FoodRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class FoodService {

    @Autowired
    FoodRepository foodRepository;

    public List<Food>
            foodList = new ArrayList<>(Arrays.asList());

    public List<Food> getAllFoods(){

        return foodRepository.findAll();
    }
    public Optional<Food> getFood(UUID id){

        return foodRepository.findById(id);
    }
    public Food addFood(Food food){
        UUID id = UUID.randomUUID();
        food.setId(id);
        foodRepository.save(food);
        return food;
    }
    public void deleteFood(UUID id){

        foodRepository.deleteById(id);
    }
}