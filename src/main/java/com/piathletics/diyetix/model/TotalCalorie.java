package com.piathletics.diyetix.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TotalCalorie {

    private int portion;
    private String name;
    private double foodCalorie;
    private String imageURL;
    
}
