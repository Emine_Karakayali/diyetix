package com.piathletics.diyetix.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {

    @Id
    private UUID id;

    @NotNull(message = "userName cannot be null.")
    @Size(min = 2, max = 15, message = "Name must be 2-15 characters.")
    private String userName;

    @NotNull(message = "password cannot be null.")
    @Size(min = 8, max = 15, message = "Name must be 8-15 characters.")
    private String password;

    @NotNull(message = "firstName cannot be null.")
    @Size(min = 2, max = 15, message = "Name must be 2-15 characters.")
    private String firstName;

    @NotNull(message = "lastName cannot be null.")
    @Size(min = 2, max = 15, message = "Name must be 2-15 characters.")
    private String lastName;


}