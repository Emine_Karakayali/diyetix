package com.piathletics.diyetix.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Food {

    private UUID id;
    private String name;
    private int calorie;
    private double counter;
    //Yorum satırı

}