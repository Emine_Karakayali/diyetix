package com.piathletics.diyetix.controller;

import com.piathletics.diyetix.dto.LoginDTO;
import com.piathletics.diyetix.dto.UserDTO;
import com.piathletics.diyetix.model.User;
import com.piathletics.diyetix.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@CrossOrigin
@RestController
@RequestMapping("/users")
@Slf4j
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping("/list")
    public ResponseEntity<List<User>> userList(){
        log.info("[GetAllUsers] 'Get Users' request was received.");
        return ResponseEntity.ok(userService.listAllUser());
    }

    @PostMapping("/add")
    ResponseEntity<UserDTO> addUser(@Valid @RequestBody UserDTO user){
        log.info("[PostUser] 'Post user' request was received.");
        userService.addUser(user);
        return ResponseEntity.status(HttpStatus.CREATED).body(user);
    }

    @PostMapping("/login")
    boolean loginUser(@RequestBody LoginDTO loginDTO) {
        return userService.userByUserName(loginDTO);
    }
    @PutMapping("/update")
    public String updateUser(@RequestBody UserDTO user){
        return userService.updateUser(user);
    }


    @DeleteMapping("/delete/{id}")
    public void userDeleteById(@PathVariable UUID id){
        log.info("[DeleteUser] 'Delete user' request was received.");
        userService.deleteUserById(id);
    }
}