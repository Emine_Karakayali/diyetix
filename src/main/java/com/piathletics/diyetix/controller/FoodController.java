package com.piathletics.diyetix.controller;

import com.piathletics.diyetix.model.Food;
import com.piathletics.diyetix.service.FoodService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@CrossOrigin
@RestController
@RequestMapping("/food")
@Slf4j
public class FoodController {
    @Autowired
    FoodService foodService;

    @GetMapping("/getAll")
    public ResponseEntity<List<Food>> getAllFoods() {
        log.info("[GetAllFoods] 'Get foods' request was received.");
        return ResponseEntity.ok(foodService.getAllFoods());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Optional<Food>> getFood(@PathVariable("id") UUID id) {
        log.info("[GetFood] 'Get food with id' request was received.");
        return ResponseEntity.ok(foodService.getFood(id));
    }

    @PostMapping("/add")
    public ResponseEntity<Food> addFood(@RequestBody Food food) {
        log.info("[PostFood] 'Post food' request was received.");
        return ResponseEntity.ok(foodService.addFood(food));
    }

    @DeleteMapping("/{id}")
    public void deleteFood(@PathVariable("id") UUID id) {
        log.info("[DeleteFood] 'Delete food' request was received.");
        foodService.deleteFood(id);
    }
}