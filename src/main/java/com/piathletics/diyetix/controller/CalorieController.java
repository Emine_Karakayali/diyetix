package com.piathletics.diyetix.controller;

import com.piathletics.diyetix.dto.CalorieDTO;
import com.piathletics.diyetix.dto.TotalCalorieDTO;
import com.piathletics.diyetix.repository.FoodRepository;
import com.piathletics.diyetix.service.CalorieService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping("/calorie")
public class CalorieController {

    @Autowired
    FoodRepository foodRepository;
    private final CalorieService calorieService;

    @PostMapping("/calculate")
    public List<TotalCalorieDTO> calculateTC(@RequestBody List<CalorieDTO> bmidto) {
        return calorieService.calculateTC(bmidto);
    }
}