import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/services/product.service';
import { ProductItem } from 'src/app/models/product-items';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  productList: ProductItem[] = [];
  searchKey: string = '';
  loading = false;

  constructor(private _productService: ProductService) { }

  ngOnInit(): void {
    this.getData();
  }

  getData(){
    this._productService.getAllProduct().subscribe(response => {
      this.productList = response;
      console.log('response', response);
    }, err => {

    });
  }
}
