import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';

@Component({
  selector: 'app-bmi',
  templateUrl: './bmi.component.html',
  styleUrls: ['./bmi.component.css']
})
export class BmiComponent implements OnInit {
  //@ViewChild('Boy') boy: ElementRef | undefined;
  //@ViewChild('Kilo') kilo: ElementRef | undefined;

  total:string = "" ;
  constructor() { }
  ngOnInit(): void {
  }

  bmi(boy: string, kilo: string):void {
    console.log(boy);
    let b:number = parseInt(boy);
    let k:number = parseInt(kilo);
    let bmi = k / (b/100 * b/100);

    if (bmi<18)
    {
      this.total="ZAYIF \nVücut kitle indexiniz: " + bmi.toString();
    }
    else if(bmi<25)
    {
      this.total="NORMAL \nVücut kitle indexiniz: " + bmi.toString();
    }
    else if(bmi<30)
    {
      this.total="KİLOLU \nVücut kitle indexiniz: " + bmi.toString();
    }
    else if(bmi<35)
    {
      this.total="OBEZ \nVücut kitle indexiniz: " + bmi.toString();
    }
    console.log(b);
  }
}


