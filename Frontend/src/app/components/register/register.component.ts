import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent implements OnInit {
  title = "Register Page - Welcome";

  _registerForm!: FormGroup;

  constructor(private _formBuilder: FormBuilder) {

  }

  ngOnInit(): void {
    this._createForm();
  }

  _createForm() {
    this._registerForm = this._formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
      isRemember: [false]
    });
  }

  signIn() {
    if (this._registerForm.valid) {
    //this._router.navigate(['/home']);
    } else {
      console.log('Form değerleri geçerli değil.')
    }
  }
}
