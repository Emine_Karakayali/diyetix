import { Pipe, PipeTransform } from "@angular/core";
import { ProductItem } from "../models/product-items";

@Pipe({ name: 'productFilter' })
export class ProductFilterPipe implements PipeTransform {

  transform(items: any[], searchText: string): any[] {
    if (!items) {
      return [];
    }
    if (!searchText) {
      return items;
    }
    searchText = searchText.toLocaleLowerCase();

    return items.filter(it => {
      return it.name.toLocaleLowerCase().includes(searchText);
    });
  }
}
